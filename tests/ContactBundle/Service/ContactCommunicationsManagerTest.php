<?php

namespace Tests\ContactBundle\Service;

use ContactBundle\Service\ContactCommunications;
use PHPUnit\Framework\TestCase;

class ContactCommunicationsManagerTest extends TestCase
{
    private $mockLogger;
    private $contactCommunicationsManager;
    
    public function setUp() {
        //$mockLogger = $this->getMock('LoggerInterface');
        $this->contactCommunicationsManager = new ContactCommunications();
    }
    
    public function tearDown() {
        $this->contactCommunicationsManager = null;
    }
    
    public function testInvalidLogEntry()
    {
        $entry = "S611222333     14200                        05012016220000";

        $result = $this->contactCommunicationsManager->validateEntry($entry);
        
        $this->assertFalse($result);
    }
    
    public function testValidLogEntry() {
        $entry = "C6112223336009998880Pepe                    01012016205203000142";
        
        $result = $this->contactCommunicationsManager->validateEntry($entry);
        
        $this->assertTrue($result);
    }
    
        
    public function communicationsLogProvider() {
        return array(
                array(file_get_contents(__DIR__.'/resources/communications.log'))
                );
    }
    
    /**
     * @dataProvider communicationsLogProvider
     */
    public function testGetCallMatches($communitcationLog) {
        $matches = $this->contactCommunicationsManager->getCallMatches($communitcationLog);
        
        $this->assertEquals(count($matches), 7);
    }
    
    /**
     * @dataProvider communicationsLogProvider
     */
    public function testGetSMSMatches($communitcationLog) {
        $matches = $this->contactCommunicationsManager->getSMSMatches($communitcationLog);
        
        $this->assertEquals(count($matches), 2);
    }
    
    public function testUnexistentCommunicationLog() {
        $msisdn = "687878787";
        
        $communitcationLog = $this->contactCommunicationsManager->getCommunicationLogByMsisdn($msisdn);
        
        $this->assertEmpty($communitcationLog);
    }
    
    public function testExistentCommunicationLog() {
        $msisdn = "611222333";
        
        $communitcationLog = $this->contactCommunicationsManager->getCommunicationLogByMsisdn($msisdn);
        
        $this->assertNotEmpty($communitcationLog);
    }
}