<?php

namespace ContactBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ContactBundle\Service\ContactCommunicationsManager;

class ContactController extends Controller
{
    private $contactCommunicationsManager;
    
    /**
     * @Route("/users/{msisdn}/communications")
     */
    public function indexAction($msisdn)
    {
        $contactCommunicationsManager = $this->get("contact.communications.manager");
        $communicationsLog = $contactCommunicationsManager->getCommunicationLogByMsisdn($msisdn);
        if ($communicationsLog != "") {
            $callMatches = $contactCommunicationsManager->getCallMatches($msisdn);
            $smsMatches = $contactCommunicationsManager->getSMSMatches($msisdn);
            return $this->render('ContactBundle:Default:index.html.twig', array('callMatches'=>$callMatches, 'smsMatches'=>$smsMatches));
        } else {
            return $this->render('ContactBundle:Exception:error404.html.twig');
        }
    }
}
