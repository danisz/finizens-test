<?php

namespace ContactBundle\Service\dto;

class CallEntry {
    
    public $callerMsisdn;
    public $calleeMsisdn;
    public $callDirection; // 0 -> outbound; 1 -> inbound
    public $contactName;
    public $date;
    public $callLength;
    
    public function __construct($callerMsisdn, $calleeMsisdn, $callDirection, $contactName, $date, $callLength) {
        $this->$callerMsisdn = $callerMsisdn;
        $this->$calleeMsisdn = $calleeMsisdn;
        $this->$callDirection = $callDirection;
        $this->$contactName = $contactName;
        $this->$date = $date;
        $this->$callLength = $callLength;
    }
    
    public function getCallerMsisdn(){
        return $this->callerMsisdn;
    }
    
    public function getCalleeMsisdn(){
        return $this->calleeMsisdn;
    }
    
    public function getCallDirection(){
        return $this->callDirection;
    }
    
    public function getContactName(){
        return $this->contactName;
    }
    
    public function getDate(){
        return $this->date;
    }
    
    public function getCallLenght(){
        return $this->callLength;
    }
    
    public function __toString() {
        return "callerMsisdn= " . $this->$callerMsisdn . " ,calleMsisdn= " . $this->$calleeMsisdn . " , callDirection= " . $this->$callDirection . " ,contactName= " . $this->$conmtactName . " ,date= " . $this->$date . " ,callLength= " . $this->$callLength;
    }
}