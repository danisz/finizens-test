<?php

namespace ContactBundle\Service;

use ContactBundle\Service\dto\CallEntry;
use ContactBundle\Service\dto\SMSEntry;
use Psr\Log\LoggerInterface;

class ContactCommunications implements ContactCommunicationsManager {
    
    const VALID_ENTRY_PATTERN = "/^([C|S])(\d{18})([\w+ *]{25})(\w{14,20})/";
    const CALL_PATTERN = "/C(\d{9})(\d{9})([0|1])([\w+ *]{24})(\d{14})(\d{6})/";
    const SMS_PATTERN = "/S(\d{9})(\d{9})([0|1])([\w+ *]{24})(\d{14})/"; 
    const REMOTE_SERVER = "https://gist.githubusercontent.com/rodrigm/8d9c2f79d637c4e0673c85f1da365ae3/raw/16ccd81dbaa895d44ac05190626de84169722700";
    
    private $logger;
    
    /**public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }*/
    
    /**
     * 
     *  @ returns true if the entry is valid, otherwise false.
     */
    function validateEntry($entry) {
        return preg_match(self::VALID_ENTRY_PATTERN, $entry) == 1;
    }
    
    /**
     * Given a string returns all the matches found based on a call pattern
     * 
     * @returns Array of CallEntry if any match exists, otherwise an empty array
     */
    function getCallMatches($communicationsLog) {
       preg_match_all(self::CALL_PATTERN, $communicationsLog, $matches, PREG_SET_ORDER);
       $callEntries = [];
       foreach ($matches as $match) {
           $callEntry = new CallEntry($match[1], $match[2], $match[3], $match[4], $match[5], $match[6]);
           array_push($callEntries, $callEntry);
       }
       //$this->logger->info("Call entries found: ".count($callEntries));
       return $callEntries;
    }
    
    /**
     * Given a string returns all the matches found based on a SMS pattern
     * 
     * @returns Array of SMSEntry if any match exists, otherwise an empty array
     */
    function getSMSMatches($communicationsLog) {
       preg_match_all(self::SMS_PATTERN, $communicationsLog, $matches, PREG_SET_ORDER);
       $smsEntries = [];
       foreach ($matches as $match) {
           $smsEntry = new SMSEntry($match[1], $match[2], $match[3], $match[4], $match[5]);
           array_push($smsEntries, $smsEntry);
       }
       //$this->logger->info("SMS entries found: ".count($smsEntries));
       return $matches;
    }
    
    /**
     * For a given msisdn retrieves its communication log from a remote server.
     * 
     * @returns a string containing all the communications for the given msisdn if exists the log, otherwise an empty string
     */
    function getCommunicationLogByMsisdn($msisdn){
        $communicationsLog = @file_get_contents(self::REMOTE_SERVER . "/communications." . $msisdn . ".log");
        if($communicationsLog == FALSE) {
            //$this->logger->warn("Communications log not found for msisdn ".$msisdn);
            $communicationsLog = "";
        }
        return $communicationsLog;
    }
}