<?php

namespace ContactBundle\Service;

interface ContactCommunicationsManager {

    /**
     * 
     *  @ returns true if the entry is valid, otherwise false.
     */
    function validateEntry($entry);
    
    /**
     * Given a string returns all the matches found based on a call pattern
     * 
     * @returns Array of CallEntry if any match exists, otherwise an empty array
     */
    function getCallMatches($communicationsLog);
    
    /**
     * Given a string returns all the matches found based on a SMS pattern
     * 
     * @returns Array of SMSEntry if any match exists, otherwise an empty array
     */
    function getSMSMatches($communicationsLog);
    
    /**
     * For a given msisdn retrieves its communication log from a remote server.
     * 
     * @returns a string containing all the communications for the given msisdn if exists the log, otherwise an empty string
     */
    function getCommunicationLogByMsisdn($msisdn);
}